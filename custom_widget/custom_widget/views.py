from django.shortcuts import render
from .forms import CustomWidgetForm


def home(request):
    if request.GET:
        form = CustomWidgetForm(request.GET) 
    else:
        form = CustomWidgetForm()

    context = {
        'form': form
    }

    return render(
        request,
        'home.html',
        context=context
    )
