from django import forms
from . import widgets


countries = [
    ('id', 'Indonesia'),
    ('sar', 'Saudi Arabia'),
    ('usa', 'United Stated')
]

hobbies = [
    ('fishing', 'Fishing'),
    ('writing', 'Writing'),
    ('coding', 'Coding')
]


class CustomWidgetForm(forms.Form):
    image = forms.FileField(
        widget=widgets.DropzoneWidget(
            options={
                'url': '/hello',
                'addRemoveLinks': True,
                'dictDefaultMessage': 'Upload your image here'
            },
        ),
        required=False
    )

    country = forms.ChoiceField(
        choices=countries,
        widget=widgets.Select2Widget()
    )
    # make sure to use multipleCHoiceFIeld if you want to use multiple selection
    # otherwise, you will get an error
    hobby = forms.MultipleChoiceField(
        choices=hobbies,
        widget=widgets.Select2MultipleWidget(
            options={
                'placeholder':'Your placeholder',
                'multiple':True,
                'maximum-selection-length': 1
            }
        )
    )

    working = forms.BooleanField(
        # required must be false, otherwise you will get error when the toggle is off
        # at least in chrome
        required=False,
        widget=widgets.ToggleWidget(
            options={
                'on': 'Yep',
                'off': 'Nope'
            }
        )
    )
