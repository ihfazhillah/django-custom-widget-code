import json

from django import forms
from django.template.loader import get_template, render_to_string
from django.utils.safestring import mark_safe


class Select2Mixin():
    class Media:
        css = {
            'all': ("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css",)
        }
        js = ("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js",
              'customselect2.js')

    def update_attrs(self, options, attrs):
        attrs = self.fix_class(attrs)
        multiple = options.pop('multiple', False)
        attrs['data-adapt-container-css-class'] = 'true'

        if multiple:
            attrs['multiple'] = 'true'

        for key, val in options.items():
            attrs['data-{}'.format(key)] = val

        return attrs

    def fix_class(self, attrs):
        class_name = attrs.pop('class', '')
        if class_name:
            class_name = '{} {}'.format(class_name, 'custom-select2-widget')
        else:
            class_name = 'custom-select2-widget'
        attrs['class'] = class_name

        return attrs


class Select2Widget(Select2Mixin, forms.Select):

    def __init__(self, attrs=None, choices=(), *args, **kwargs):

        attrs = attrs or {}
        options = kwargs.pop('options', {})
        new_attrs = self.update_attrs(options, attrs)

        super().__init__(new_attrs)
        self.choices = list(choices)


class Select2MultipleWidget(Select2Mixin, forms.widgets.SelectMultiple):
    def __init__(self, attrs=None, choices=(), *args, **kwargs):

        attrs = attrs or {}
        options = kwargs.pop('options', {})
        new_attrs = self.update_attrs(options, attrs)

        super().__init__(new_attrs)
        self.choices = list(choices)


class ToggleWidget(forms.widgets.CheckboxInput):
    class Media:
        css = {'all': (
            "https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css", )}
        js = ("https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js",)

    def __init__(self, attrs=None, *args, **kwargs):
        attrs = attrs or {}

        default_options = {
            'toggle': 'toggle',
            'offstyle': 'danger'
        }
        options = kwargs.get('options', {})
        default_options.update(options)
        for key, val in default_options.items():
            attrs['data-' + key] = val

        super().__init__(attrs)


class DropzoneWidget(forms.widgets.FileInput):
    template_name = 'forms/widgets/dropzone.html'

    class Media:
        js = ("https://rawgit.com/enyo/dropzone/master/dist/dropzone.js", 'customdz.js')
        css = {
            'all': ("https://rawgit.com/enyo/dropzone/master/dist/dropzone.css",)
        }

    def __init__(self, attrs=None, options={}):
        self.options = options

        super().__init__(attrs)

    def get_context(self, name, value, attrs):
        context = {}
        current_class = attrs.get('class')
        custom_class = 'custom-dropzone-widget-' + name

        if current_class:
            attrs['class'] = current_class + ' ' + custom_class
        else:
            attrs['class'] = custom_class

        attrs = self.build_attrs(attrs, name=name)
        self.options.update({
            'class': custom_class,
            'paramName': name
        })

        context['widget'] = {
            'name': name,
            'attrs': self.build_attrs(extra_attrs=attrs),
        }

        context['options'] = json.dumps(self.options)
        return context

    def render(self, name, value, attrs={}):
        context = self.get_context(name, value, attrs)
        return mark_safe(render_to_string(self.template_name,
                                          context))
