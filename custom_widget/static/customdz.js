  Dropzone.autoDiscover = false;
  $(document).ready(() => {
    $('[id^="custom-dropzone-widget"]').each((index, element) => {
      options = window[element.id]
      className = 'div.' + options.class
      new Dropzone(className, options)
    })
  })

